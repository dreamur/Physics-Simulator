# Physics-Simulator

This is a pretty straightforward application. It mostly showcases basic mechanics, and how a 'normal' sized object moves when it's accelerated by gravity and so forth.

No directions are really necessary, just play around with the buttons and you'll be able to figure things out.
