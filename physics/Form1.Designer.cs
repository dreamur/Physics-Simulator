﻿namespace physics
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {  
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.stage = new System.Windows.Forms.Panel();
            this.overlay = new System.Windows.Forms.Panel();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.ramp_back = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.ramp_base = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.ramp_hyp = new Microsoft.VisualBasic.PowerPacks.LineShape();
            this.actr_Tank = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.actr_Box = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.actr_Ball = new Microsoft.VisualBasic.PowerPacks.OvalShape();
            this.actr_btn_Density = new System.Windows.Forms.Button();
            this.spot_light_incr_Ypos = new System.Windows.Forms.NumericUpDown();
            this.spot_light_incr_ypos_Label = new System.Windows.Forms.Label();
            this.spot_light_incr_Xpos = new System.Windows.Forms.NumericUpDown();
            this.spot_light_incr_xpos_Label = new System.Windows.Forms.Label();
            this.spot_light_button_Toggle = new System.Windows.Forms.Button();
            this.lbl_spot_Light = new System.Windows.Forms.Label();
            this.glob_light_btn_Toggle = new System.Windows.Forms.Button();
            this.lbl_glob_Light = new System.Windows.Forms.Label();
            this.tank_incr_Height = new System.Windows.Forms.NumericUpDown();
            this.tank_incr_height_Label = new System.Windows.Forms.Label();
            this.tank_incr_Xpos = new System.Windows.Forms.NumericUpDown();
            this.tank_incr_xpos_Label = new System.Windows.Forms.Label();
            this.tank_btn_Toggle = new System.Windows.Forms.Button();
            this.labl_Tank = new System.Windows.Forms.Label();
            this.ramp_incr_Ypos = new System.Windows.Forms.NumericUpDown();
            this.ramp_incr_ypos_Label = new System.Windows.Forms.Label();
            this.ramp_incr_Xpos = new System.Windows.Forms.NumericUpDown();
            this.ramp_incr_xpos_Label = new System.Windows.Forms.Label();
            this.ramp_incr_theta_Label = new System.Windows.Forms.Label();
            this.actr_btn_density_Label = new System.Windows.Forms.Label();
            this.ramp_incr_Theta = new System.Windows.Forms.NumericUpDown();
            this.ramp_btn_Toggle = new System.Windows.Forms.Button();
            this.actr_btn_Ball = new System.Windows.Forms.Button();
            this.actr_btn_Box = new System.Windows.Forms.Button();
            this.labl_Ramp = new System.Windows.Forms.Label();
            this.labl_Actor = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.stage.SuspendLayout();
            this.overlay.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spot_light_incr_Ypos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spot_light_incr_Xpos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tank_incr_Height)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tank_incr_Xpos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramp_incr_Ypos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramp_incr_Xpos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramp_incr_Theta)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.splitContainer1.Panel1.Controls.Add(this.stage);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.splitContainer1.Panel2.Controls.Add(this.actr_btn_Density);
            this.splitContainer1.Panel2.Controls.Add(this.spot_light_incr_Ypos);
            this.splitContainer1.Panel2.Controls.Add(this.spot_light_incr_ypos_Label);
            this.splitContainer1.Panel2.Controls.Add(this.spot_light_incr_Xpos);
            this.splitContainer1.Panel2.Controls.Add(this.spot_light_incr_xpos_Label);
            this.splitContainer1.Panel2.Controls.Add(this.spot_light_button_Toggle);
            this.splitContainer1.Panel2.Controls.Add(this.lbl_spot_Light);
            this.splitContainer1.Panel2.Controls.Add(this.glob_light_btn_Toggle);
            this.splitContainer1.Panel2.Controls.Add(this.lbl_glob_Light);
            this.splitContainer1.Panel2.Controls.Add(this.tank_incr_Height);
            this.splitContainer1.Panel2.Controls.Add(this.tank_incr_height_Label);
            this.splitContainer1.Panel2.Controls.Add(this.tank_incr_Xpos);
            this.splitContainer1.Panel2.Controls.Add(this.tank_incr_xpos_Label);
            this.splitContainer1.Panel2.Controls.Add(this.tank_btn_Toggle);
            this.splitContainer1.Panel2.Controls.Add(this.labl_Tank);
            this.splitContainer1.Panel2.Controls.Add(this.ramp_incr_Ypos);
            this.splitContainer1.Panel2.Controls.Add(this.ramp_incr_ypos_Label);
            this.splitContainer1.Panel2.Controls.Add(this.ramp_incr_Xpos);
            this.splitContainer1.Panel2.Controls.Add(this.ramp_incr_xpos_Label);
            this.splitContainer1.Panel2.Controls.Add(this.ramp_incr_theta_Label);
            this.splitContainer1.Panel2.Controls.Add(this.actr_btn_density_Label);
            this.splitContainer1.Panel2.Controls.Add(this.ramp_incr_Theta);
            this.splitContainer1.Panel2.Controls.Add(this.ramp_btn_Toggle);
            this.splitContainer1.Panel2.Controls.Add(this.actr_btn_Ball);
            this.splitContainer1.Panel2.Controls.Add(this.actr_btn_Box);
            this.splitContainer1.Panel2.Controls.Add(this.labl_Ramp);
            this.splitContainer1.Panel2.Controls.Add(this.labl_Actor);
            this.splitContainer1.Size = new System.Drawing.Size(715, 600);
            this.splitContainer1.SplitterDistance = 460;
            this.splitContainer1.TabIndex = 0;
            // 
            // stage
            // 
            this.stage.BackColor = System.Drawing.Color.Snow;
            this.stage.Controls.Add(this.overlay);
            this.stage.Location = new System.Drawing.Point(3, 3);
            this.stage.Name = "stage";
            this.stage.Size = new System.Drawing.Size(454, 594);
            this.stage.TabIndex = 0;
            // 
            // overlay
            // 
            this.overlay.BackColor = System.Drawing.Color.Transparent;
            this.overlay.Controls.Add(this.shapeContainer1);
            this.overlay.ForeColor = System.Drawing.Color.Transparent;
            this.overlay.Location = new System.Drawing.Point(0, -3);
            this.overlay.Name = "overlay";
            this.overlay.Size = new System.Drawing.Size(454, 597);
            this.overlay.TabIndex = 0;
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.ramp_back,
            this.ramp_base,
            this.ramp_hyp,
            this.actr_Tank,
            this.actr_Box,
            this.actr_Ball});
            this.shapeContainer1.Size = new System.Drawing.Size(454, 597);
            this.shapeContainer1.TabIndex = 0;
            this.shapeContainer1.TabStop = false;
            // 
            // ramp_back
            // 
            this.ramp_back.BorderWidth = 3;
            this.ramp_back.Name = "ramp_back";
            this.ramp_back.Visible = false;
            this.ramp_back.X1 = 30;
            this.ramp_back.X2 = 30;
            this.ramp_back.Y1 = 229;
            this.ramp_back.Y2 = 308;
            // 
            // ramp_base
            // 
            this.ramp_base.BorderWidth = 3;
            this.ramp_base.Name = "ramp_base";
            this.ramp_base.Visible = false;
            this.ramp_base.X1 = 31;
            this.ramp_base.X2 = 168;
            this.ramp_base.Y1 = 306;
            this.ramp_base.Y2 = 307;
            // 
            // ramp_hyp
            // 
            this.ramp_hyp.BorderWidth = 3;
            this.ramp_hyp.Name = "ramp_hyp";
            this.ramp_hyp.Visible = false;
            this.ramp_hyp.X1 = 30;
            this.ramp_hyp.X2 = 170;
            this.ramp_hyp.Y1 = 230;
            this.ramp_hyp.Y2 = 310;
            // 
            // actr_Tank
            // 
            this.actr_Tank.BorderWidth = 3;
            this.actr_Tank.FillColor = System.Drawing.Color.White;
            this.actr_Tank.FillGradientColor = System.Drawing.Color.SteelBlue;
            this.actr_Tank.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.actr_Tank.Location = new System.Drawing.Point(271, 315);
            this.actr_Tank.Name = "actr_Tank";
            this.actr_Tank.Size = new System.Drawing.Size(185, 285);
            this.actr_Tank.Visible = false;
            // 
            // actr_Box
            // 
            this.actr_Box.BorderColor = System.Drawing.Color.Black;
            this.actr_Box.BorderWidth = 3;
            this.actr_Box.FillColor = System.Drawing.Color.Beige;
            this.actr_Box.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.actr_Box.Location = new System.Drawing.Point(187, 30);
            this.actr_Box.Name = "actr_Box";
            this.actr_Box.Size = new System.Drawing.Size(50, 50);
            this.actr_Box.Visible = false;
            // 
            // actr_Ball
            // 
            this.actr_Ball.BorderColor = System.Drawing.Color.Black;
            this.actr_Ball.BorderWidth = 3;
            this.actr_Ball.FillColor = System.Drawing.Color.Beige;
            this.actr_Ball.FillGradientColor = System.Drawing.Color.Yellow;
            this.actr_Ball.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.actr_Ball.Location = new System.Drawing.Point(52, 24);
            this.actr_Ball.Name = "actr_Ball";
            this.actr_Ball.Size = new System.Drawing.Size(50, 50);
            this.actr_Ball.Visible = false;
            // 
            // actr_btn_Density
            // 
            this.actr_btn_Density.Location = new System.Drawing.Point(137, 76);
            this.actr_btn_Density.Name = "actr_btn_Density";
            this.actr_btn_Density.Size = new System.Drawing.Size(75, 23);
            this.actr_btn_Density.TabIndex = 27;
            this.actr_btn_Density.Text = "High";
            this.actr_btn_Density.UseVisualStyleBackColor = true;
            this.actr_btn_Density.Click += new System.EventHandler(this.actr_btn_Density_Click);
            // 
            // spot_light_incr_Ypos
            // 
            this.spot_light_incr_Ypos.Location = new System.Drawing.Point(137, 553);
            this.spot_light_incr_Ypos.Maximum = new decimal(new int[] {
            638,
            0,
            0,
            0});
            this.spot_light_incr_Ypos.Name = "spot_light_incr_Ypos";
            this.spot_light_incr_Ypos.Size = new System.Drawing.Size(75, 20);
            this.spot_light_incr_Ypos.TabIndex = 26;
            // 
            // spot_light_incr_ypos_Label
            // 
            this.spot_light_incr_ypos_Label.AutoSize = true;
            this.spot_light_incr_ypos_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.spot_light_incr_ypos_Label.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.spot_light_incr_ypos_Label.Location = new System.Drawing.Point(19, 553);
            this.spot_light_incr_ypos_Label.Name = "spot_light_incr_ypos_Label";
            this.spot_light_incr_ypos_Label.Size = new System.Drawing.Size(76, 20);
            this.spot_light_incr_ypos_Label.TabIndex = 25;
            this.spot_light_incr_ypos_Label.Text = "Up-Down";
            // 
            // spot_light_incr_Xpos
            // 
            this.spot_light_incr_Xpos.Location = new System.Drawing.Point(137, 526);
            this.spot_light_incr_Xpos.Maximum = new decimal(new int[] {
            731,
            0,
            0,
            0});
            this.spot_light_incr_Xpos.Name = "spot_light_incr_Xpos";
            this.spot_light_incr_Xpos.Size = new System.Drawing.Size(75, 20);
            this.spot_light_incr_Xpos.TabIndex = 24;
            // 
            // spot_light_incr_xpos_Label
            // 
            this.spot_light_incr_xpos_Label.AutoSize = true;
            this.spot_light_incr_xpos_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.spot_light_incr_xpos_Label.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.spot_light_incr_xpos_Label.Location = new System.Drawing.Point(19, 526);
            this.spot_light_incr_xpos_Label.Name = "spot_light_incr_xpos_Label";
            this.spot_light_incr_xpos_Label.Size = new System.Drawing.Size(80, 20);
            this.spot_light_incr_xpos_Label.TabIndex = 23;
            this.spot_light_incr_xpos_Label.Text = "Left-Right";
            // 
            // spot_light_button_Toggle
            // 
            this.spot_light_button_Toggle.Location = new System.Drawing.Point(23, 500);
            this.spot_light_button_Toggle.Name = "spot_light_button_Toggle";
            this.spot_light_button_Toggle.Size = new System.Drawing.Size(189, 23);
            this.spot_light_button_Toggle.TabIndex = 22;
            this.spot_light_button_Toggle.Text = "Toggle Spot Light";
            this.spot_light_button_Toggle.UseVisualStyleBackColor = true;
            // 
            // lbl_spot_Light
            // 
            this.lbl_spot_Light.AutoSize = true;
            this.lbl_spot_Light.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.lbl_spot_Light.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lbl_spot_Light.Location = new System.Drawing.Point(3, 465);
            this.lbl_spot_Light.Name = "lbl_spot_Light";
            this.lbl_spot_Light.Size = new System.Drawing.Size(136, 31);
            this.lbl_spot_Light.TabIndex = 21;
            this.lbl_spot_Light.Text = "Spot Light";
            // 
            // glob_light_btn_Toggle
            // 
            this.glob_light_btn_Toggle.BackColor = System.Drawing.Color.Transparent;
            this.glob_light_btn_Toggle.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.glob_light_btn_Toggle.ForeColor = System.Drawing.SystemColors.ControlText;
            this.glob_light_btn_Toggle.Location = new System.Drawing.Point(23, 417);
            this.glob_light_btn_Toggle.Name = "glob_light_btn_Toggle";
            this.glob_light_btn_Toggle.Size = new System.Drawing.Size(189, 23);
            this.glob_light_btn_Toggle.TabIndex = 20;
            this.glob_light_btn_Toggle.Text = "Toggle Global Light";
            this.glob_light_btn_Toggle.UseVisualStyleBackColor = false;
            this.glob_light_btn_Toggle.Click += new System.EventHandler(this.glob_light_btn_Toggle_Click);
            // 
            // lbl_glob_Light
            // 
            this.lbl_glob_Light.AutoSize = true;
            this.lbl_glob_Light.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.lbl_glob_Light.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.lbl_glob_Light.Location = new System.Drawing.Point(3, 382);
            this.lbl_glob_Light.Name = "lbl_glob_Light";
            this.lbl_glob_Light.Size = new System.Drawing.Size(158, 31);
            this.lbl_glob_Light.TabIndex = 19;
            this.lbl_glob_Light.Text = "Global Light";
            // 
            // tank_incr_Height
            // 
            this.tank_incr_Height.Location = new System.Drawing.Point(137, 343);
            this.tank_incr_Height.Maximum = new decimal(new int[] {
            596,
            0,
            0,
            0});
            this.tank_incr_Height.Name = "tank_incr_Height";
            this.tank_incr_Height.Size = new System.Drawing.Size(75, 20);
            this.tank_incr_Height.TabIndex = 18;
            this.tank_incr_Height.Value = new decimal(new int[] {
            285,
            0,
            0,
            0});
            this.tank_incr_Height.ValueChanged += new System.EventHandler(this.tank_incr_Height_ValueChanged);
            // 
            // tank_incr_height_Label
            // 
            this.tank_incr_height_Label.AutoSize = true;
            this.tank_incr_height_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.tank_incr_height_Label.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.tank_incr_height_Label.Location = new System.Drawing.Point(17, 343);
            this.tank_incr_height_Label.Name = "tank_incr_height_Label";
            this.tank_incr_height_Label.Size = new System.Drawing.Size(56, 20);
            this.tank_incr_height_Label.TabIndex = 17;
            this.tank_incr_height_Label.Text = "Height";
            // 
            // tank_incr_Xpos
            // 
            this.tank_incr_Xpos.Location = new System.Drawing.Point(137, 319);
            this.tank_incr_Xpos.Maximum = new decimal(new int[] {
            271,
            0,
            0,
            0});
            this.tank_incr_Xpos.Name = "tank_incr_Xpos";
            this.tank_incr_Xpos.Size = new System.Drawing.Size(75, 20);
            this.tank_incr_Xpos.TabIndex = 16;
            this.tank_incr_Xpos.Value = new decimal(new int[] {
            271,
            0,
            0,
            0});
            this.tank_incr_Xpos.ValueChanged += new System.EventHandler(this.tank_incr_Xpos_ValueChanged);
            // 
            // tank_incr_xpos_Label
            // 
            this.tank_incr_xpos_Label.AutoSize = true;
            this.tank_incr_xpos_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.tank_incr_xpos_Label.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.tank_incr_xpos_Label.Location = new System.Drawing.Point(17, 319);
            this.tank_incr_xpos_Label.Name = "tank_incr_xpos_Label";
            this.tank_incr_xpos_Label.Size = new System.Drawing.Size(80, 20);
            this.tank_incr_xpos_Label.TabIndex = 15;
            this.tank_incr_xpos_Label.Text = "Left-Right";
            // 
            // tank_btn_Toggle
            // 
            this.tank_btn_Toggle.Location = new System.Drawing.Point(23, 290);
            this.tank_btn_Toggle.Name = "tank_btn_Toggle";
            this.tank_btn_Toggle.Size = new System.Drawing.Size(189, 23);
            this.tank_btn_Toggle.TabIndex = 14;
            this.tank_btn_Toggle.Text = "Toggle Liquid Tank";
            this.tank_btn_Toggle.UseVisualStyleBackColor = true;
            this.tank_btn_Toggle.Click += new System.EventHandler(this.tank_btn_Toggle_Click);
            // 
            // labl_Tank
            // 
            this.labl_Tank.AutoSize = true;
            this.labl_Tank.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.labl_Tank.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labl_Tank.Location = new System.Drawing.Point(3, 256);
            this.labl_Tank.Name = "labl_Tank";
            this.labl_Tank.Size = new System.Drawing.Size(75, 31);
            this.labl_Tank.TabIndex = 13;
            this.labl_Tank.Text = "Tank";
            // 
            // ramp_incr_Ypos
            // 
            this.ramp_incr_Ypos.Location = new System.Drawing.Point(137, 221);
            this.ramp_incr_Ypos.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
            this.ramp_incr_Ypos.Minimum = new decimal(new int[] {
            85,
            0,
            0,
            0});
            this.ramp_incr_Ypos.Name = "ramp_incr_Ypos";
            this.ramp_incr_Ypos.Size = new System.Drawing.Size(75, 20);
            this.ramp_incr_Ypos.TabIndex = 12;
            this.ramp_incr_Ypos.Value = new decimal(new int[] {
            370,
            0,
            0,
            0});
            this.ramp_incr_Ypos.ValueChanged += new System.EventHandler(this.ramp_incr_Ypos_ValueChanged);
            // 
            // ramp_incr_ypos_Label
            // 
            this.ramp_incr_ypos_Label.AutoSize = true;
            this.ramp_incr_ypos_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ramp_incr_ypos_Label.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.ramp_incr_ypos_Label.Location = new System.Drawing.Point(19, 221);
            this.ramp_incr_ypos_Label.Name = "ramp_incr_ypos_Label";
            this.ramp_incr_ypos_Label.Size = new System.Drawing.Size(76, 20);
            this.ramp_incr_ypos_Label.TabIndex = 11;
            this.ramp_incr_ypos_Label.Text = "Up-Down";
            // 
            // ramp_incr_Xpos
            // 
            this.ramp_incr_Xpos.Location = new System.Drawing.Point(137, 194);
            this.ramp_incr_Xpos.Maximum = new decimal(new int[] {
            315,
            0,
            0,
            0});
            this.ramp_incr_Xpos.Name = "ramp_incr_Xpos";
            this.ramp_incr_Xpos.Size = new System.Drawing.Size(75, 20);
            this.ramp_incr_Xpos.TabIndex = 10;
            this.ramp_incr_Xpos.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.ramp_incr_Xpos.ValueChanged += new System.EventHandler(this.ramp_incr_Xpos_ValueChanged);
            // 
            // ramp_incr_xpos_Label
            // 
            this.ramp_incr_xpos_Label.AutoSize = true;
            this.ramp_incr_xpos_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ramp_incr_xpos_Label.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.ramp_incr_xpos_Label.Location = new System.Drawing.Point(19, 194);
            this.ramp_incr_xpos_Label.Name = "ramp_incr_xpos_Label";
            this.ramp_incr_xpos_Label.Size = new System.Drawing.Size(80, 20);
            this.ramp_incr_xpos_Label.TabIndex = 9;
            this.ramp_incr_xpos_Label.Text = "Left-Right";
            // 
            // ramp_incr_theta_Label
            // 
            this.ramp_incr_theta_Label.AutoSize = true;
            this.ramp_incr_theta_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ramp_incr_theta_Label.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.ramp_incr_theta_Label.Location = new System.Drawing.Point(19, 168);
            this.ramp_incr_theta_Label.Name = "ramp_incr_theta_Label";
            this.ramp_incr_theta_Label.Size = new System.Drawing.Size(55, 20);
            this.ramp_incr_theta_Label.TabIndex = 8;
            this.ramp_incr_theta_Label.Text = "Incline";
            // 
            // actr_btn_density_Label
            // 
            this.actr_btn_density_Label.AutoSize = true;
            this.actr_btn_density_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.actr_btn_density_Label.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.actr_btn_density_Label.Location = new System.Drawing.Point(19, 72);
            this.actr_btn_density_Label.Name = "actr_btn_density_Label";
            this.actr_btn_density_Label.Size = new System.Drawing.Size(62, 20);
            this.actr_btn_density_Label.TabIndex = 7;
            this.actr_btn_density_Label.Text = "Density";
            // 
            // ramp_incr_Theta
            // 
            this.ramp_incr_Theta.Location = new System.Drawing.Point(137, 168);
            this.ramp_incr_Theta.Maximum = new decimal(new int[] {
            70,
            0,
            0,
            0});
            this.ramp_incr_Theta.Name = "ramp_incr_Theta";
            this.ramp_incr_Theta.Size = new System.Drawing.Size(75, 20);
            this.ramp_incr_Theta.TabIndex = 6;
            this.ramp_incr_Theta.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.ramp_incr_Theta.ValueChanged += new System.EventHandler(this.ramp_incr_Theta_ValueChanged);
            // 
            // ramp_btn_Toggle
            // 
            this.ramp_btn_Toggle.Location = new System.Drawing.Point(23, 139);
            this.ramp_btn_Toggle.Name = "ramp_btn_Toggle";
            this.ramp_btn_Toggle.Size = new System.Drawing.Size(189, 23);
            this.ramp_btn_Toggle.TabIndex = 5;
            this.ramp_btn_Toggle.Text = "Toggle Ramp";
            this.ramp_btn_Toggle.UseVisualStyleBackColor = true;
            this.ramp_btn_Toggle.Click += new System.EventHandler(this.ramp_btn_Toggle_Click);
            // 
            // actr_btn_Ball
            // 
            this.actr_btn_Ball.Location = new System.Drawing.Point(137, 40);
            this.actr_btn_Ball.Name = "actr_btn_Ball";
            this.actr_btn_Ball.Size = new System.Drawing.Size(75, 23);
            this.actr_btn_Ball.TabIndex = 3;
            this.actr_btn_Ball.Text = "Ball";
            this.actr_btn_Ball.UseVisualStyleBackColor = true;
            this.actr_btn_Ball.Click += new System.EventHandler(this.actr_btn_Ball_Click);
            // 
            // actr_btn_Box
            // 
            this.actr_btn_Box.Location = new System.Drawing.Point(21, 40);
            this.actr_btn_Box.Name = "actr_btn_Box";
            this.actr_btn_Box.Size = new System.Drawing.Size(82, 23);
            this.actr_btn_Box.TabIndex = 2;
            this.actr_btn_Box.Text = "Box";
            this.actr_btn_Box.UseVisualStyleBackColor = true;
            this.actr_btn_Box.Click += new System.EventHandler(this.actr_btn_Box_Click);
            // 
            // labl_Ramp
            // 
            this.labl_Ramp.AutoSize = true;
            this.labl_Ramp.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.labl_Ramp.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labl_Ramp.Location = new System.Drawing.Point(3, 105);
            this.labl_Ramp.Name = "labl_Ramp";
            this.labl_Ramp.Size = new System.Drawing.Size(86, 31);
            this.labl_Ramp.TabIndex = 1;
            this.labl_Ramp.Text = "Ramp";
            // 
            // labl_Actor
            // 
            this.labl_Actor.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F);
            this.labl_Actor.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.labl_Actor.Location = new System.Drawing.Point(3, 9);
            this.labl_Actor.Name = "labl_Actor";
            this.labl_Actor.Size = new System.Drawing.Size(182, 63);
            this.labl_Actor.TabIndex = 0;
            this.labl_Actor.Text = "Actors";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.ClientSize = new System.Drawing.Size(715, 600);
            this.Controls.Add(this.splitContainer1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Form1";
            this.Text = "Form1";
            this.TopMost = true;
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.stage.ResumeLayout(false);
            this.overlay.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spot_light_incr_Ypos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spot_light_incr_Xpos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tank_incr_Height)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tank_incr_Xpos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramp_incr_Ypos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramp_incr_Xpos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ramp_incr_Theta)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label labl_Actor;
        private System.Windows.Forms.Label labl_Ramp;
        private System.Windows.Forms.Button actr_btn_Ball;
        private System.Windows.Forms.Button actr_btn_Box;
        private System.Windows.Forms.Label actr_btn_density_Label;
        private System.Windows.Forms.NumericUpDown ramp_incr_Theta;
        private System.Windows.Forms.Button ramp_btn_Toggle;
        private System.Windows.Forms.Label ramp_incr_theta_Label;
        private System.Windows.Forms.NumericUpDown ramp_incr_Xpos;
        private System.Windows.Forms.Label ramp_incr_xpos_Label;
        private System.Windows.Forms.NumericUpDown ramp_incr_Ypos;
        private System.Windows.Forms.Label ramp_incr_ypos_Label;
        private System.Windows.Forms.Panel stage;
        private System.Windows.Forms.NumericUpDown tank_incr_Height;
        private System.Windows.Forms.Label tank_incr_height_Label;
        private System.Windows.Forms.NumericUpDown tank_incr_Xpos;
        private System.Windows.Forms.Label tank_incr_xpos_Label;
        private System.Windows.Forms.Button tank_btn_Toggle;
        private System.Windows.Forms.Label labl_Tank;
        private System.Windows.Forms.Label lbl_glob_Light;
        private System.Windows.Forms.NumericUpDown spot_light_incr_Ypos;
        private System.Windows.Forms.Label spot_light_incr_ypos_Label;
        private System.Windows.Forms.NumericUpDown spot_light_incr_Xpos;
        private System.Windows.Forms.Label spot_light_incr_xpos_Label;
        private System.Windows.Forms.Button spot_light_button_Toggle;
        private System.Windows.Forms.Label lbl_spot_Light;
        private System.Windows.Forms.Button glob_light_btn_Toggle;
        private System.Windows.Forms.Panel overlay;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.OvalShape actr_Ball;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape actr_Box;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape actr_Tank;
        private Microsoft.VisualBasic.PowerPacks.LineShape ramp_back;
        private Microsoft.VisualBasic.PowerPacks.LineShape ramp_base;
        private Microsoft.VisualBasic.PowerPacks.LineShape ramp_hyp;
        private System.Windows.Forms.Button actr_btn_Density;
    }
}

