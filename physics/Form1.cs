﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace physics
{
    public partial class Form1 : Form
    {
        System.Windows.Forms.Timer myTimer = new System.Windows.Forms.Timer();
        bool box_current_actor;
        bool ball_current_actor;
        int acceleration;
        int x_velocity, y_velocity;                 /* the 'vector' used to track the object's motion */
        int x_pos, y_pos;                           /* the x, y position of the object's top-left corner */
        int water_dampening;                        /* used to decrease the velocity of an object in a liquid */
        int friction_force;                         /* also used to slow the x component of a velocity vector */
        bool density;                               /* used to determine whether the object sinks or floats */
        int term_velocity;                          /* maximum velocity */
        bool float_flag;

        int initial_x;
        int initial_y;

        public Form1()
        {
            InitializeComponent();
            myTimer.Tick += TimerEventProcessor;                /* this essentially says "for each tick (0.5 sec -> see below), do whatever is within the timerEventProcessor method" */
            myTimer.Interval = 500;                             /*  set the timer's event method to fire every 500 ms (0.5 sec) */
            myTimer.Start();                                    /*  start the timer */
            acceleration = -5;                                  /*  initialize acceleration neg five */
            box_current_actor = false;                          /*  this flag must be 'global' - it keeps track if the ball/box has been swapped for acceleration resetting */
            ball_current_actor = false;                         
            x_pos = 52; y_pos = 24; x_velocity = 0; y_velocity = 0;   /* initialize position and acceleration vector */
            water_dampening = 2;
            density = true;                                     /* true -> float; false -> sink */
            term_velocity = 25;
            friction_force = 2;
            float_flag = false;
            initial_x = 52;
            initial_y = 24;
        }

        private void TimerEventProcessor(Object myObject, EventArgs myEventArgs)
        {
            if (actr_Box.Visible == true || actr_Ball.Visible == true)
            {
                int motion_vector = calc_motion_vector(acceleration, x_velocity, y_velocity);

                int magnitude = motion_vector;

                Tuple<int, int> minor_tuple = calc_hops(acceleration, magnitude);

                /* this is taken care of in the calc_hops method now */
                int passes = minor_tuple.Item1;

                while (passes > 0)
                {
                    update_position(x_velocity, y_velocity, magnitude);
                    correct_collisions();

                    /* force invalidation and redraw in one fell swoop */
                    this.Refresh();

                    --passes;
                }
                y_velocity -= acceleration;

                if (y_velocity > term_velocity) { y_velocity = term_velocity; }
                if (x_velocity > term_velocity) { x_velocity = term_velocity; }
            }
        }

        /***
         *
         * used so that I'll know if the ball or the box is active
         *   
         *  returns 0 if neither are visible
         *  returns 1 if ball
         *  returns 2 if box
         *
         **/
        private int return_current_actor()
        {
            return ball_current_actor == true ? 1 : (box_current_actor == true) ? 2 : 0;
        }

        /***
         *
         * gets the coordinates of the visible actor
         *
         *  since the coordinates should never be negative, we can return negatives on
         *  an exception
         *
         **/
        private Tuple<int, int> get_current_actor_coords(int param_current_actor)
        {
            if (param_current_actor == 1) { Tuple<int, int> return_tup = new Tuple<int, int>(actr_Ball.Location.X, actr_Ball.Location.Y); return return_tup; }
            else if (param_current_actor == 2) { Tuple<int, int> return_tup = new Tuple<int, int>(actr_Box.Location.X, actr_Box.Location.Y); return return_tup; }
            else { Tuple<int, int> return_tup = new Tuple<int, int>(-100, -100); return return_tup;  }
        }

        /***
         *
         * assigns members their original values given to them during initialization
         *
         **/
         private void reset_actor_data_members()
         {
            x_pos = 52; y_pos = 24; x_velocity = 0; y_velocity = 0; initial_x = 52; initial_y = 24; 
         }

        /***
         *
         * calculates the final velocity component given the initial velocity component and
         *  total displacement.
         *
         *  note that the displacement and the Vi parameters must be for the same axis
         *
         **/
         private int calc_result_velocity(int initial_velocity, int displacement)
         {
            /* modeled after kinematic equation: Vf^2 = Vi^2 + 2ad */
            return (int)Math.Sqrt( initial_velocity*initial_velocity + 2 * acceleration * displacement * -1);
         }

        /***
         * 
         * helper fn for the correct_collisions method
         *  this keeps the visible actor within the bounds of the stage
         * 
         **/
         private void correct_out_of_bounds()
         {
            Tuple<int, int> bounds_tuple = get_current_actor_coords(return_current_actor());
             if (bounds_tuple.Item1 != -100)
             {
                 /* if the bottom of the object's Y-level is touching or below stage's floor...*/
                 if (bounds_tuple.Item2 + 50 > stage.Size.Height)
                 {
                     /* reset the object's position */
                    if (return_current_actor() == 1) { actr_Ball.Location = new Point(bounds_tuple.Item1, stage.Size.Height - 50); }
                    else if (return_current_actor() == 2) { actr_Box.Location = new Point(bounds_tuple.Item1, stage.Size.Height - 50); }
                     
                     y_velocity *= -1;

                     /* add friction to slow object to a stop */
                     if (y_velocity == 0)
                     {
                         if (Math.Abs(x_velocity) > friction_force)
                         {
                            x_velocity = x_velocity > 0 ? (x_velocity - friction_force) : x_velocity < 0 ? (x_velocity + friction_force) : x_velocity;
                         }
                         else { x_velocity = 0; }
                     }
                 }

                 /* if the the object's left side is touching or hidden by the stage's left wall...*/
                 if (bounds_tuple.Item1 < 0)
                 {
                     if (return_current_actor() == 1) { actr_Ball.Location = new Point(1, bounds_tuple.Item2); }
                     else if (return_current_actor() == 2) { actr_Box.Location = new Point(1, bounds_tuple.Item2); }
                     
                     x_velocity *= -1;
                 }

                 /* if the the object's right side is touching or hidden by the stage's right wall...*/
                 if (bounds_tuple.Item1 + 50 > stage.Size.Width)
                 {
                    if (return_current_actor() == 1) { actr_Ball.Location = new Point(stage.Size.Width - 50 - 1, bounds_tuple.Item2); }
                    else if (return_current_actor() == 2) { actr_Box.Location = new Point(stage.Size.Width - 50 - 1, bounds_tuple.Item2); }

                     x_velocity *= -1;
                 }

                 /* if the the object's top touching or hidden by the stage's top...*/
                 if (bounds_tuple.Item2 < 0)
                 {
                    if (return_current_actor() == 1) { actr_Ball.Location = new Point(bounds_tuple.Item1, 1); }
                     else if (return_current_actor() == 2) { actr_Box.Location = new Point(bounds_tuple.Item1, 1); }

                     y_velocity *= -1;
                 }
             }    
         }

        /***
         * 
         * a fourth helper fn for the correct_collisions method
         *  this checks if the ramp is within the object's collision box
         * 
         **/
         private void correct_ramp_collisions()
         {
            Tuple<int, int> ramp_tuple = get_current_actor_coords(return_current_actor());
            if (ramp_tuple.Item1 != -100)
            {
                 double m = (double)(ramp_hyp.Y2 - ramp_hyp.Y1) / (double)(ramp_hyp.X2 - ramp_hyp.X1);
                 double b = ramp_hyp.Y2 / (m * ramp_hyp.X2);
                 int y = (int)(m * ramp_tuple.Item1 + b);

                 /* if the bottom of the object's Y-level is between the ramp's hypotenuse's y-level...*/
                 if (ramp_tuple.Item2 + 50 >= ramp_hyp.Y1 + y && ramp_tuple.Item2 + 50 <= ramp_base.Y2)
                 {
                     /* if the left side of the ball's X-Pos is between the ramp's hypotenuse's x-pos...*/
                     if (ramp_tuple.Item1 >= ramp_back.X1 && ramp_tuple.Item1 <= ramp_base.X2)
                     {
                         /* ... this means the ball is colliding with the hypotenuse */

                        if (return_current_actor() == 1) { actr_Ball.Location = new Point(ramp_tuple.Item1, ramp_hyp.Y1 + y - 50); }
                        else if (return_current_actor() == 2) { actr_Box.Location = new Point(ramp_tuple.Item1, ramp_hyp.Y1 + y - 50); }
                         
                         int result_y = calc_result_velocity(y_velocity, ramp_tuple.Item2 - initial_y);
                         int result_x = calc_result_velocity(x_velocity, ramp_tuple.Item1 - initial_x);

                         /* split the two vectors into their components and add the respective components */
                         int temp = (int)( result_y * Math.Sin(60 * (Math.PI / 180)) + result_x * Math.Sin(60 * (Math.PI / 180)) );
                         result_x = (int)( result_x * Math.Cos(60 * (Math.PI / 180)) + result_y * Math.Cos(60 * (Math.PI / 180)) );

                         /***
                          *
                          * if the ball is going to bounce off the ramp, then the resultant can be subtracted from the original y velocity
                          * 
                          * likewise, the resultant can be added to the x velocity
                          *
                          **/
                         y_velocity = -1 * temp;
                         x_velocity = result_x;
                     }
                 }
            }
         }

         /***
          * 
          * another helper fn for correct_collisions method
          *  this checks to see if actor has collided with tank wall
          * 
          **/
         private bool correct_tank_wall_outer_collision()
         {
            Tuple<int, int> outer_tuple = get_current_actor_coords(return_current_actor());
            if (outer_tuple.Item1 != -100)
            {
                /* if the ball runs into the tank from the left */
                if (x_velocity > 0 && outer_tuple.Item1 + 50 > actr_Tank.Location.X && outer_tuple.Item2 >= actr_Tank.Location.Y && outer_tuple.Item1 < actr_Tank.Location.X + actr_Tank.Size.Width)
                {
                    if (return_current_actor() == 1) { actr_Ball.Location = new Point(actr_Tank.Location.X - 50 - 1, outer_tuple.Item2); }
                    else if (return_current_actor() == 2) { actr_Box.Location = new Point(actr_Tank.Location.X - 50 - 1, outer_tuple.Item2); }

                    x_velocity *= -1;
                    return true;
                }
                /* if the ball runs into the tank from the right */
                if (x_velocity < 0 && outer_tuple.Item1 < actr_Tank.Location.X + actr_Tank.Size.Width && outer_tuple.Item2 >= actr_Tank.Location.Y && outer_tuple.Item1 + 50 > actr_Tank.Location.X + actr_Tank.Size.Width)
                {
                    if (return_current_actor() == 1) { actr_Ball.Location = new Point(actr_Tank.Location.X + actr_Tank.Size.Width + 50 + 1, outer_tuple.Item2); }
                    else if (return_current_actor() == 2) { actr_Box.Location = new Point(actr_Tank.Location.X + actr_Tank.Size.Width + 50 + 1, outer_tuple.Item2); }

                    x_velocity *= -1;
                    return true;
                }
                else { return false; }
            }
            else { return false; }
         }

         /***
          * 
          * a third helper fn for the correct_collisions method
          *  this handles collisions against the walls within the tank
          * 
          **/
         private bool correct_tank_wall_inner_collision()
         {
            Tuple<int, int> inner_tuple = get_current_actor_coords(return_current_actor());
            if (inner_tuple.Item1 != -100)
            {
                /* if the object runs into the tank's right wall from the inside */
                if (x_velocity > 0 && inner_tuple.Item1 + 50 > actr_Tank.Location.X + actr_Tank.Size.Width && inner_tuple.Item1 < actr_Tank.Location.X + actr_Tank.Size.Width && inner_tuple.Item2 >= actr_Tank.Location.Y)
                {
                    if (return_current_actor() == 1) { actr_Ball.Location = new Point(actr_Tank.Location.X + actr_Tank.Size.Width - 50, inner_tuple.Item2); }
                    else if (return_current_actor() == 2) { actr_Box.Location = new Point(actr_Tank.Location.X + actr_Tank.Size.Width - 50, inner_tuple.Item2); }

                    x_velocity *= -1;
                    return true;
                }
                /* if the object runs into the tank's left wall from the inside */
                else if (x_velocity < 0 && inner_tuple.Item1 < actr_Tank.Location.X && inner_tuple.Item1 + 50 > actr_Tank.Location.X && inner_tuple.Item2 >= actr_Tank.Location.Y)
                {
                    if (return_current_actor() == 1) { actr_Ball.Location = new Point(actr_Tank.Location.X, inner_tuple.Item2); }
                    else if (return_current_actor() == 2) { actr_Box.Location = new Point(actr_Tank.Location.X, inner_tuple.Item2); }

                    x_velocity *= -1;
                    return true;
                }
                else { return false; }
            }
            else { return false; }
         }

        /***
         * 
         * another helper fn for correct_collisions
         *  this just does a few minor tweaks on the two velocity components whenever
         *  the object is within the liquid
         * 
         **/
         private void correct_tank_liquid_collision()
         {
            Tuple<int, int> liquid_tuple = get_current_actor_coords(return_current_actor());
            if (liquid_tuple.Item1 != -100)
            {
                if (liquid_tuple.Item1 > actr_Tank.Location.X && liquid_tuple.Item1 + 50 < actr_Tank.Location.X + actr_Tank.Size.Width && liquid_tuple.Item2 >= actr_Tank.Location.Y)
                 {
                     if (Math.Abs(x_velocity) > water_dampening)
                     {
                         x_velocity = x_velocity > 0 ? (x_velocity - water_dampening) : x_velocity < 0 ? (x_velocity + water_dampening) : x_velocity;
                     }
                     else
                     {
                         x_velocity = 0;
                     }
                     /* set the ball to sink */
                     if (density == false)
                     {
                         if (Math.Abs(y_velocity) > water_dampening)
                         {
                             y_velocity = y_velocity > 0 ? (y_velocity - water_dampening) : y_velocity < 0 ? (y_velocity + water_dampening) : y_velocity;
                         }
                         else
                         {
                             y_velocity = 0;
                         }
                    }    
                    else if (density == true)
                    {
                        /* if entire object is within liquid... */
                        if (liquid_tuple.Item2 > actr_Tank.Location.Y)
                        {
                            /*** 
                             *
                             * we adjust the y_velocity so that the object slows down over time
                             *
                             **/
                            if (y_velocity < 0 && !float_flag)
                            {
                                y_velocity = y_velocity - acceleration - water_dampening;

                                if (y_velocity < 0)
                                {
                                    float_flag = true;
                                }
                            }
                            else if (y_velocity > 0 && float_flag)
                            {
                                y_velocity = y_velocity - acceleration - water_dampening;

                                if (y_velocity > 0)
                                {
                                    float_flag = false;
                                }
                            }
                        }
                        /* if the object is partly submerged... */
                        else if (liquid_tuple.Item2 < actr_Tank.Location.Y && liquid_tuple.Item2 + 50 / 2 > actr_Tank.Location.Y)
                        {
                            if (y_velocity > 0)
                            {
                                y_velocity = y_velocity + acceleration - water_dampening;
                            }
                            else if (y_velocity < 0)
                            {
                                y_velocity = y_velocity - acceleration + water_dampening;
                            }
                        }
                        else
                        {
                            y_velocity = 0;
                        }
                    }          
                 }
            }
                           
         }

        /***
         * 
         * checks the boundaries of the ball, box, ramp, tank and floor. If certain conditions
         *  are met, a collision is handled
         *  
         * no returns necessary - this is the last thing that update position does. so that the
         *  position can be adjusted before drawing
         * 
         **/
        private void correct_collisions()
        {            
            if (ball_current_actor == true || box_current_actor == true)
            {
                if (ramp_base.Visible == true)
                {
                    correct_ramp_collisions();
                }
                if (actr_Tank.Visible == true)
                {
                    if (correct_tank_wall_outer_collision() == false)
                    {
                        correct_tank_wall_inner_collision();
                        correct_tank_liquid_collision();
                    }   
                }

                correct_out_of_bounds();                              
            }
        } /* end return-collisions method */

        /***
         * 
         * performs a single update on the object's x_pos & y_pos members
         * 
         **/
        private void update_position(int param_x_vel, int param_y_vel, int param_magnitude)
        {
            /*** 
             * 
             * you add the positive velocity and the acceleration
             *  i.e. downward velocity and
             *  the magnitude of the acceleration (by subtracting the negative)
             *  
             **/
            if (param_y_vel != 0) { y_pos = y_pos + param_y_vel - acceleration; }
            if (param_x_vel != 0) { x_pos += param_x_vel; }

            /* update the actor's coordinates */
            if (ball_current_actor == true) { actr_Ball.Location = new Point(x_pos, y_pos); }
            else if (box_current_actor == true) { actr_Box.Location = new Point(x_pos, y_pos); }           
        }

        /***
         * 
         * used once within the TimerEventProcessor method to calculate the
         *  number of 'passes' the primary loop does upon each event trigger
         *  
         * this returns a tuple;
         *  Item1 is the number of passes
         *  Item2 is the length of the final hop
         *      if there is no final hop (mag mod accel eq zero), then this is zero
         *
         * Note: this used to return the length of each hop as well. This is no longer
         *  necessary; the new standard length is just the magnitude of 'g' - 5.
         * 
         **/
        private Tuple<int, int> calc_hops(int param_acceleration, int param_magnitude)
        {
            int hops = param_magnitude / Math.Abs(param_acceleration);      // we ignore sign of accel, it'd mess things up
            int remainder = param_magnitude % param_acceleration;

            if (remainder > 0)
            {
                Tuple<int, int> return_tuple = new Tuple<int, int>(hops + 1, remainder);
                return return_tuple;
            }
            else
            {
                Tuple<int, int> return_tuple = new Tuple<int, int>(hops, remainder);
                return return_tuple;
            }            
        }

        /***
         *
         * used to calculate the resultant vector of the object's motion path after
         *  each timer event fires
         *  
         * this returns the magnitude of the motion vector; 
         * 
         * note that the component/directions are calculated later
         * 
         **/
        private int calc_motion_vector(int param_accel, int param_x_vel, int param_y_vel)
        {
            return (int)Math.Sqrt(param_x_vel * param_x_vel + param_y_vel * param_y_vel);
        }

        private void glob_light_btn_Toggle_Click(object sender, EventArgs e)
        {
            /* set to light yellow with transparency */
            if (overlay.BackColor == Color.Transparent) { overlay.BackColor = Color.FromArgb(0x7fFFDEAD); }
            /* set to light blue with transparency */
            else if (overlay.BackColor == Color.FromArgb(0x7fFFDEAD)) { overlay.BackColor = Color.FromArgb(0x7f00bfff); }
            else { overlay.BackColor = Color.Transparent; }             
        }

        private void actr_btn_Box_Click(object sender, EventArgs e)
        {
            if (actr_Box.Visible == false)
            {
                reset_actor_data_members();
                actr_Box.Visible = true;              
                actr_Box.Location = new Point(x_pos, y_pos);
                box_current_actor = true;

                if (actr_Ball.Visible == true) { actr_Ball.Visible = false; }
            }
            else { actr_Box.Visible = false; box_current_actor = false; }
        }

        private void actr_btn_Ball_Click(object sender, EventArgs e)
        {
            if (actr_Ball.Visible == false)
            {
                reset_actor_data_members();
                actr_Ball.Visible = true;
                actr_Ball.Location = new Point(x_pos, y_pos);
                ball_current_actor = true;

                if (actr_Box.Visible == true) { actr_Box.Visible = false; }
            }
            else { actr_Ball.Visible = false; ball_current_actor = false; }
        }

        /***
         * 
         * toggles visibility of tank (also resets location upon viewing)
         * 
         **/
        private void tank_btn_Toggle_Click(object sender, EventArgs e)
        {
            if (actr_Tank.Visible == false)
            {
                actr_Tank.Visible = true;
                actr_Tank.Location = new Point(271, 315);
                actr_Tank.Size = new Size(185, 285);
                actr_Tank.FillColor = Color.FromArgb(0x446a5acd);
            }
            else actr_Tank.Visible = false;
        }

        /***
         * 
         * adjusts the x-position of the tank 
         *  according to the value of the
         *  tank_incr_Xpos control
         * 
         **/
        private void tank_incr_Xpos_ValueChanged(object sender, EventArgs e)
        {
            actr_Tank.Location = new Point((int)tank_incr_Xpos.Value, actr_Tank.Location.Y);
        }

        /***
         * 
         * adjusts the height of the 'tank' on the form
         *  makes certain to adjust the y-position of the
         *  tank as well.
         * 
         **/
        private void tank_incr_Height_ValueChanged(object sender, EventArgs e)
        {
            /* if the tank is getting taller, scoot it up on the screen */
            if (tank_incr_Height.Value > actr_Tank.Size.Height)
            {
                actr_Tank.Location = new Point(actr_Tank.Location.X, actr_Tank.Location.Y - Math.Abs((int)tank_incr_Height.Value - actr_Tank.Size.Height));
                actr_Tank.Size = new Size(actr_Tank.Size.Width, (int)tank_incr_Height.Value);
            }
            /* if the tank is shrinking, resize it and scoot it down */
            else if (tank_incr_Height.Value < actr_Tank.Size.Height)
            {
                actr_Tank.Location = new Point(actr_Tank.Location.X, actr_Tank.Location.Y + Math.Abs((int)tank_incr_Height.Value - actr_Tank.Size.Height));
                actr_Tank.Size = new Size(actr_Tank.Size.Width, (int)tank_incr_Height.Value);
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            myTimer.Stop();
        }

        /**
         * 
         * resets location of the ramp
         *  and also hides ramp lines if they're visible
         *  else makes ramp lines visible
         *  
         **/
        private void ramp_btn_Toggle_Click(object sender, EventArgs e)
        {
            if (ramp_hyp.Visible == false)
            {
                ramp_hyp.Visible = true;
                ramp_hyp.X1 = 30;     ramp_hyp.X2 = 170;
                ramp_hyp.Y1 = 230;    ramp_hyp.Y2 = 310;
            }
            else { ramp_hyp.Visible = false; }
            
            if (ramp_back.Visible == false)
            {
                ramp_back.Visible = true;
                ramp_back.X1 = 30;    ramp_back.X2 = 30;
                ramp_back.Y1 = 230;   ramp_back.Y2 = 310;
            }
            else { ramp_back.Visible = false; }

            if (ramp_base.Visible == false)
            {
                ramp_base.Visible = true;
                ramp_base.X1 = 30;    ramp_base.X2 = 170;
                ramp_base.Y1 = 310;   ramp_base.Y2 = 310;
            }
            else { ramp_base.Visible = false; }           
        }



        /**
         *
         * moves ramp up or down
         * 
         **/ 
        private void ramp_incr_Ypos_ValueChanged(object sender, EventArgs e)
        {
            /* we take 600 minus the up/down value. this converts the up/down's value to a working value */
            int difference = ramp_hyp.Y1 - (600 - (int)ramp_incr_Ypos.Value);

            if (difference == 0) return;
            /* move it down; a negative difference means that the new value is greater than the old one */
            if (difference > 0)
            {
                /* subtract the difference in heights from the bottom location */
                ramp_hyp.Y2 -= difference;     ramp_hyp.Y1 -= difference;
                ramp_back.Y2 -= difference;    ramp_back.Y1 -= difference;
                ramp_base.Y1 -= difference;    ramp_base.Y2 -= difference;
            }
            /* move it up; a positive difference means that the new value is less than the old one */
            else if (difference < 0)
            {
                ramp_hyp.Y2 -= difference;     ramp_hyp.Y1 -= difference;
                ramp_back.Y2 -= difference;    ramp_back.Y1 -= difference;
                ramp_base.Y1 -= difference;    ramp_base.Y2 -= difference;
            }
            
        }

        /**
         * 
         * moves the ramp left or right
         * 
         **/
        private void ramp_incr_Xpos_ValueChanged(object sender, EventArgs e)
        {
            /* we take 600 minus the up/down value. this converts the up/down's value to a working value */
            int difference = ramp_hyp.X1 - (int)ramp_incr_Xpos.Value;

            if (difference == 0) return;

            /* move it down; a negative difference means that the new value is greater than the old one */
            if (difference > 0)
            {
                /* subtract the difference in heights from the bottom location */
                ramp_hyp.X2 -= difference;    ramp_hyp.X1 -= difference;
                ramp_back.X2 -= difference;   ramp_back.X1 -= difference;
                ramp_base.X1 -= difference;   ramp_base.X2 -= difference;
            }
            /* move it up; a positive difference means that the new value is less than the old one */
            else if (difference < 0)
            {
                ramp_hyp.X2 -= difference;   ramp_hyp.X1 -= difference;
                ramp_back.X2 -= difference;  ramp_back.X1 -= difference;
                ramp_base.X1 -= difference;  ramp_base.X2 -= difference;
            }                  
        }

        /**
         * 
         * changes the angle (theta) of the ramp by using a bunch of trig.
         *  note that the base of the ramp never changes size, only the
         *  hypotenuse and the back.
         * 
         **/
        private void ramp_incr_Theta_ValueChanged(object sender, EventArgs e)
        {
            /* capture the angle and convert it to radians first (because trig fns use rads) */
            /* also, apparently x div 180 * pi != x * (pi div 180); the order below is required */
            double angle = (int)ramp_incr_Theta.Value * (Math.PI / 180);
            double height = Math.Tan(angle) * (ramp_base.X2 - ramp_base.X1);

            /* take care of the difference in the height segment */
            int diff_height = (int)(height) - (ramp_back.Y2 - ramp_back.Y1);
            /* if the new height is greater; we'll move the new height up accordingly */
            if (diff_height != 0)
            {
                ramp_back.Y1 -= diff_height;
                ramp_hyp.Y1 -= diff_height;
            }
        }

        /**
         * 
         * the event method for the Density button.
         *  this changes the value of the desity property
         * 
         **/
        private void actr_btn_Density_Click(object sender, EventArgs e)
        {
            if (actr_btn_Density.Text == "High")
            {
                actr_btn_Density.Text = "Low";
                density = true;
            }
            else if (actr_btn_Density.Text == "Low")
            {
                actr_btn_Density.Text = "High";
                density = false;
            }
        }

    } /* end of Form1 class */
} /* end of physics namespace */
